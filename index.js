const puppeteer = require('puppeteer');

const url = "https://tremplinprepabac.fr/projets/sciences-medecine-recherche/ingenieure-en-intelligence-artificielle-assistant-les-personnes";
let votes = 0;

(async () => {
  const browser = await puppeteer.launch();

  setInterval(async () => {
    const page = await browser.newPage();
    await page._client.send('Network.clearBrowserCookies');
    await page.goto(url);
    await page.click('.action-flag');
    votes++;
    console.log(`+${votes} votes`);
    await page.close();
  }, 1300);

  //browser.close();
})();
